<?php

namespace App;
use Teacher;
use App\Student;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name', 'description', 'day', 'price', 'discount','sale_price', 'start_time', 'end_time', 'thumbnail',
    ];

    /**
     * Set the categories
     *
     */

    public function setCatAttribute($value)
    {
        $this->attributes['day'] = json_encode($value);
    }

    /**
     * Get the categories
     *
     */

    public function getCatAttribute($value)
    {
        return $this->attributes['day'] = json_decode($value);
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class, 'course_teacher', 'teacher_id', 'course_id');
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'course_student', 'student_id', 'course_id');
    }
}
