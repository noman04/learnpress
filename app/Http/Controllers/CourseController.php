<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseValidationRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Course;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::paginate(10);
        return view('admin.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CourseValidationRequest $request)
    {
        try {

            $course = new Course();
            $course->name = $request->name;
            $course->description = $request->description;
            $multipleSelect = $request->day;
            $course['day'] = implode(',', $multipleSelect);

            $course->price = $request->price;
            $course->discount = $request->discount;

            $course->sale_price = $course->price * (1 - $course->discount / 100);

            $startTime = $request->start_time;
            $course->start_time = Carbon::parse($startTime)->format('Y-m-d\TH:i');

            $endTime = $request->end_time;
            $course->end_time = Carbon::parse($endTime)->format('Y-m-d\TH:i');

            $filename = sprintf('thumbnail_%s.jpg', random_int(1, 1000));
            if ($request->hasFile('thumbnail'))
                $filename = $request->file('thumbnail')->storeAs('images', $filename, 'public');
            $course->thumbnail = $filename;

            $save = $course->save();

            return redirect()
                ->route('courses.index')
                ->with('success','Course Created successfully');

        }catch (\Exception $exception){

            return redirect()
                ->route('courses.index')
                ->with('error', 'There is an issue with your submission, please try again later.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::find($id);
        return view('admin.courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $course = Course::findOrFail($id);
//        $selected = explode(",", $course->course_id);
//        return view('admin.courses.edit', compact('course',selected));

        $course = Course::findOrFail($id);
        $selectDays = explode(',', $course->day);
        return view('admin.courses.edit', compact('course','selectDays'));


    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CourseValidationRequest $request, $id)
    {
        $course = Course::find($id);
        $course->name = $request->name;
        $course->description = $request->description;
        $multipleSelect = $request->day;
        $course['day'] = implode(',', $multipleSelect);

        $course->price = $request->price;
        $course->discount = $request->discount;

        $course->sale_price =  $course->price * (1 - $course->discount / 100);

        $startTime = $request->start_time;
        $course->start_time = Carbon::parse($startTime)->format('Y-m-d\TH:i');

        $endTime= $request->end_time;
        $course->end_time  =  Carbon::parse($endTime)->format('Y-m-d\TH:i');

        $filename = sprintf('thumbnail_%s.jpg', random_int(1,1000));
        if($request->hasFile('thumbnail')){
            $filename = $request->file('thumbnail')->storeAs('images', $filename,'public');
            $course->thumbnail = $filename;
        }else{
            unset($course['thumbnail']);
        }
        $save = $course->save();
        if($save){
            return redirect()->route('courses.index')->with('success','Course Updated Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Course::find($id)->delete();
        return redirect()->route('courses.index')->with('deleted','Course Deleted Successfully');

    }
}
