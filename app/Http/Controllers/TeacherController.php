<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\TeacherValidationRequest;
use Illuminate\Http\Request;
use App\Teacher;
use App\Course;
use Carbon\Carbon;
class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::paginate(10);
        $course = Course::all();
        return view('admin.teachers.index', compact('teachers', 'course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.teachers.create', compact('courses'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherValidationRequest $request)
    {
        try {
            $filename = sprintf('thumbnail_%s.jpg', random_int(1, 1000));
            if ($request->hasFile('thumbnail'))
                $filename = $request->file('thumbnail')->storeAs('teachers', $filename, 'public');

            $teacher = [
                'name' => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'description' => $request->description,
                'experience' => $request->experience,
                'address' => $request->address,
                'thumbnail' => $filename,
            ];
            $teacher = Teacher::create($teacher);
            $teacher->courses()->attach($request->course);
            if ($teacher) {
                return redirect()->route('teachers.index')->with('success', 'Teacher Created successfully');
            }
        }catch (\Exception $exception){

            return redirect()
                ->route('teachers.index')
                ->with('error', 'There is an issue with your submission, please try again later.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::find($id);
        return view('admin.teachers.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::findOrFail($id);
        $course = Course::all();
        return view('admin.teachers.edit', compact('teacher','course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teachers =  Teacher::findOrFail($id);

        $filename = sprintf('thumbnail_%s.jpg', random_int(1,1000));
        if($request->hasFile('thumbnail')){
            $filename = $request->file('thumbnail')->storeAs('teachers', $filename,'public');
        }else{
            $filename = ($teachers['thumbnail']);
        }

        $teacher = [
            'name' => $request->name,
            'email' => $request->email,
            'contact' => $request->contact,
            'description' => $request->description,
            'experience' => $request->experience,
            'address' => $request->address,
            'thumbnail' => $filename,
        ];

        $teacher = $teachers->update($teacher);

        $teachers->courses()->sync($request->course);

        if($teacher){
            return redirect()->route('teachers.index')->with('success','Teacher Updated successfully');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teacher::find($id)->delete();
        return redirect()->route('teachers.index')->with('deleted','Teacher deleted successfully');
    }
}
