<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'contact' => 'required',
            'description' => 'required',
            'course' => 'required',
            'teacher' => 'required',
            'experience' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'thumbnail' => 'required',
        ];
    }
}
