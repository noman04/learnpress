<?php

namespace App;
use App\Course;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{



    protected $fillable = [
        'teacher_id', 'name', 'email', 'contact', 'description', 'experience','dob', 'address', 'thumbnail',
    ];

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_student', 'student_id', 'course_id');
    }
}
