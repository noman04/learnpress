<?php

namespace App;
use App\Course;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{


    protected $fillable = [
        'name', 'email', 'contact', 'description', 'experience','address', 'thumbnail',
    ];



    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_teacher', 'teacher_id', 'course_id');
    }

}
