@extends('admin.layouts.app')
@section('title', 'Courses')

@push('stylesheets')

@endpush
@section('content')
    <!-- Hoverable rows start -->
    <section class="section">
        <div class="row" id="table-hover-row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <div class="row">
                           <div class="col-12 col-md-4">
                               <h3 class="text-bold-500" style="font-weight: bold">All Courses</h3>
                           </div>
                           <div class="col-12 col-md-8 text-right">
                               <a href="{{ route('courses.index') }}" class="btn btn-info">All courses</a>
                               <a href="{{ route('courses.create') }}" class="btn btn-info">Add New Course</a>
                           </div>
                       </div>
                    </div>
                    @if ($message = Session::get('deleted'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="card-content">
                        <div class="card-body">

                        </div>
                        <!-- table hover -->
                        @if(!$courses->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Orignal Price</th>
                                    <th>Discount</th>
                                    <th>Sale Price</th>
                                    <th>Price</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($courses as $course)
                                <tr>
                                    <td class="text-bold-500">{{ $course->id }}</td>
                                    <td class="text-bold-500" style="text-transform: capitalize">{{ $course->name }}</td>

                                    @if($course->discount == 0 )
                                        <td class="text-bold-500">{{ $course->price }} Rs</td>
                                    @else
                                        <td class="text-bold-500" style="color: red"><del>{{ $course->price }}</del> Rs</td>
                                    @endif


                                    <td class="text-bold-500">{{ $course->discount }}%</td>
                                    <td class="text-bold-500"  style="color: #4c4c1e">{{ $course->sale_price }} Rs</td>

                                    @if(!$course->discount == 0 )
                                        <td class="text-bold-500"  style="color: green">{{ $course->sale_price }} Rs</td>
                                    @else
                                        <td class="text-bold-500"  style="color: green">{{ $course->price }} Rs</td>
                                    @endif

                                    <td>
                                        <form action="{{ route('courses.destroy',$course->id) }}" method="POST">
                                            <a class="btn btn-info" href="{{ route('courses.show',$course->id) }}" title="Show"><i class="fas fa-eye"></i></a>
                                            <a class="btn btn-success" href="{{ route('courses.edit',$course->id) }}" title="Edit"><i class="fas fa-edit"></i></a>
                                            @csrf
                                            @method('DELETE')
                                            <button href="{{route('courses.destroy', $course->id)}}" title="Delete"  type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                            @else
                            <div class=" p-3">
                                <p class="alert text-center alert-info font-weight-bold">No course Available</p>
                            </div>

                            @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hoverable rows end -->


@endsection


@push('scripts')

@endpush
