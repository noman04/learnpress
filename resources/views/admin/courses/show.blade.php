@extends('admin.layouts.app')
@section('title', 'show')

@push('stylesheets')



@endpush
<style>
    body {
        padding: 0;
        margin: 0;
        font-family: 'Lato', sans-serif;
        color: #000;
    }

    .student-profile .card {
        border-radius: 10px;
    }

    .student-profile .card .card-header .profile_img {
        width: 150px;
        height: 150px;
        object-fit: cover;
        margin: 10px auto;
        border: 10px solid #ccc;
        border-radius: 50%;
    }

    .student-profile .card h3 {
        font-size: 20px;
        font-weight: 700;
    }

    .student-profile .card p {
        font-size: 16px;
        color: #000;
    }

    .student-profile .table th,
    .student-profile .table td {
        font-size: 14px;
        padding: 5px 10px;
        color: #000;
    }
</style>
@section('content')
    <!-- Student Profile -->



    <div class="row" style="margin-top: 50px">
        <div class="col-lg-4">
            <div class="card shadow-sm">
                <div class="card-header bg-transparent text-center">
                    <img class="profile_img img-" style="border-radius: 158px;width: 160px;height: 160px; border: 13px solid #4e4e4e61;"
                         src="{{ URL::asset('storage/'.$course->thumbnail) }}" alt="">
                    <h3 style="text-transform: capitalize; font-weight: 600"> {{$course->name}}</h3>
                </div>
                <div class="card-body" style="margin-top: 30px;">
                    <a role="button" href="{{route('courses.edit', $course->id)}}" class="text-warning btn btn-success" style="width: 100%; margin: 5px 0">Edit</a>
                    <form action="{{route('courses.destroy', $course->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button style="width: 100%; " role="button" href="{{route('courses.destroy', $course->id)}}" class="text-danger btn btn-danger font-weight-bold-700">Delete</button>
                    </form>
                    <a role="button" href="{{route('courses.index')}}" class="text-warning btn btn-info" style="width: 100%;"> <i class="fas fa-arrow-left"></i>  Back To Courses</a>

                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card shadow-sm">
                <div class="card-header bg- border-0">
                    <h3 class="mb-0" style="font-weight: bold; margin-bottom: 20px; text-align: center;background-color: #d51515; text-transform: capitalize;color: #fff;padding: 10px" ></i>
                      Course Name : <b>( {{$course->name}} )</b>  </h3>
                </div>
                <div class="card-body pt-0">
                    <table class="table table-bordered">
                        <tr>
                            <th width="30%">Days</th>
                            <td width="2%">:</td>
                            <td style="text-transform: capitalize">{{$course->day}}</td>
                        </tr>

                        <tr>
                            <th width="30%">Price</th>
                            <td width="2%">:</td>
                            <td>{{$course->price}} Rs</td>
                        </tr>
                        <tr>
                            <th width="30%">Discount</th>
                            <td width="2%">:</td>
                            <td>{{$course->discount}}%</td>
                        </tr>

                        <tr>
                            <th width="30%">Sale Price</th>
                            <td width="2%">:</td>
                            <td>{{$course->sale_price}}</td>
                        </tr>

                        <tr>
                            <th width="30%">Start Time</th>
                            <td width="2%">:</td>
                            <td>{{ Carbon\Carbon::parse($course->start_time)->format('d-M-y - g:i:s A' ) }}</td>
                        </tr>

                        <tr>
                            <th width="30%">End Time</th>
                            <td width="2%">:</td>
                            <td>{{ Carbon\Carbon::parse($course->end_time)->format('d-M-y - g:i:s A' ) }}</td>
                        </tr>
                        <tr>
                            <th width="30%">Description</th>
                            <td width="2%">:</td>
                            <td>{{$course->description}}</td>
                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')

@endpush
