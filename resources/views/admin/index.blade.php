@extends('admin.layouts.app')
@section('title', 'Courses')

@push('stylesheets')

    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            padding: 10px 20px;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
            background-color: rgba(21, 18, 18, 0.06);
        }
        .card span {
            background-color: #18183c;
            color: #fff;
            padding: 8px 16px;
            border-radius: 8px;
            margin-left: 20px;

    </style>

@endpush
@section('content')

    <div class="col-12" style="margin: 50px 0;">
        <h1 style="font-weight: bold">Admin Dashboard</h1>
    </div>

    <div class="col-12 col-lg-12">

        <div class="row">
            <div class="col-12 col-lg-12">
                <a href="{{route('courses.index')}}">
                    <div class="card">
                       <div class="row">
                           <div class="col-md-8">
                               <h2><i style="font-size: 60px;" class="fa fa-graduation-cap" aria-hidden="true"></i></h2>
                               <h4 style="font-weight: bold">All Courses</h4>
                           </div>
                           <div class="col-md-4">
                               @if($courses->count() > 0 )
                            <h4 style="font-weight: bold">Total Courses <span>
                                   {{count($courses)}}
                                </span></h4>

                               @else
                                   <h4 style="font-weight: bold">Total Courses <span>
                                   0
                                </span></h4>
                               @endif

                           </div>
                       </div>
                    </div>
                </a>
            </div>


            <div class="col-12 col-lg-12">
                <a href="{{route('teachers.index')}}">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-8">
                                <h2><i style="font-size: 60px;" class="fas fa-chalkboard-teacher"></i></h2>
                                <h4 style="font-weight: bold">All Teachers</h4>
                            </div>
                            <div class="col-md-4">
                                @if($teachers->count() > 0 )
                                    <h4 style="font-weight: bold">Total Teachers <span>
                                   {{count($teachers)}}
                                </span></h4>

                                @else
                                    <h4 style="font-weight: bold">Total Teachers <span>
                                   0
                                </span></h4>
                                @endif

                            </div>
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-12 col-lg-12">
                <a href="{{route('students.index')}}">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-8">
                                <h2><i style="font-size: 60px;" class="fa fa-child" aria-hidden="true"></i></h2>
                                <h4 style="font-weight: bold">All Students</h4>
                            </div>
                            <div class="col-md-4">
                                @if($students->count() > 0 )
                                    <h4 style="font-weight: bold">Total Students <span>
                                   {{count($students)}}
                                </span></h4>

                                @else
                                    <h4 style="font-weight: bold">Total Students <span>
                                   0
                                </span></h4>
                                @endif
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>

    </div>

@endsection
