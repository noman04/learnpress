@extends('admin.layouts.app')
@section('title', 'teacher')

@push('stylesheets')

@endpush
@section('content')

<div class="col" style="margin-top: 50px">
    <div class="row">
        <div class="col-12 col-md-4">
            <h3 class="text-bold-500" style="font-weight: bold">Add New Student</h3>
        </div>
        <div class="col-12 col-md-8 text-right">
            <a href="{{ route('students.index') }}" class="btn btn-info">All Students</a>
        </div>
    </div>
    <form action="{{ route('students.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-xs-10 col-md-4 col-lg-4">
            <label for="name">Student Name</label>
            <input type="text" class="form-control" name="name" id="name">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('name')
                {{ $message }}
                @enderror</span>
        </div>
        <div class="form-group col-xs-10  col-md-4 col-lg-4">
            <label for="name">Email</label>
            <input type="email" class="form-control" name="email" id="email">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('email')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-4 col-lg-4">
            <label for="name">Contact</label>
            <input type="text" class="form-control" name="contact" id="contact">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('contact')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-12 col-lg-12">
        <label for="comment">Description:</label>
        <textarea class="form-control" rows="3" id="description" name="description"></textarea>
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('description')
                {{ $message }}
                @enderror</span>
        </div>



        <div class="form-group col-xs-10 col-md-12 col-lg-6">
            <label for="day">Select Course</label>
                <select style="text-transform: capitalize" class="form-control course" id="course_id" name="course_id" data-dependent="teacher">
                <option value="">Select Course</option>
              @if(!$courses->isEmpty())
                    @foreach($courses as $course)
                        <option value="{{$course->id}}">{{$course->name}}</option>
                    @endforeach
                @endif
            </select>
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('course_id')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-12 col-lg-6">
            <label for="day">Select Teacher</label>
            <select  style="text-transform: capitalize" class="form-control" id="teacher" name="teacher">
                @if(!$teachers->isEmpty())
                    @foreach($teachers as $teacher)
                        <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                    @endforeach
                @endif
            </select>
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('teacher')
                {{ $message }}
                @enderror</span>

        </div>
        <div class="form-group col-xs-10 col-md-4 col-lg-6">
            <label for="name">Experience</label>
            <input type="text" class="form-control" name="experience" id="experience">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('experience')
                {{ $message }}
                @enderror</span>
        </div>
        <div class="form-group col-xs-10 col-md-4 col-lg-6">
            <label for="name">Date of birth</label>
            <input type="date" class="form-control" name="dob" id="dob">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('dob')
                {{ $message }}
                @enderror</span>
        </div>
        <div class="form-group col-xs-10 col-md-4 col-lg-6">
            <label for="name">Address</label>
            <input type="text" class="form-control" name="address" id="address">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('address')
                {{ $message }}
                @enderror</span>
        </div>
        <div class="form-group col-xs-10 col-md-4 col-lg-6">
            <label for="name">Image</label>
            <input type="file" class="form-control" name="thumbnail" id="thumbnail">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('thumbnail')
                {{ $message }}
                @enderror</span>
        </div>


        <button type="submit" class="btn btn-info " style="width: 100%">Add new Student</button>
    </form>
</div>

@endsection


@push('scripts')

    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>

    <script type="text/javascript">

        var jq = jQuery.noConflict();

        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        jq(document).ready(function () {

            jq('#course').on('change',function(e) {

                var teacher_id = e.target.value;
                jq.ajax({

{{--                    url:"{{ route('subcat') }}",--}}
                    type:"POST",
                    data: {
                        teacher_id: teacher_id
                    },

                    success:function (data) {
                        jq('#teacher').empty();
                        jq.each(data.teachers[0].teachers,function(index,teacher){

                            jq('#teacher').append('<option value="'+teacher.id+'">'+teacher.name+'</option>');
                        })
                    }
                })
            });
        });
    </script>

@endpush


