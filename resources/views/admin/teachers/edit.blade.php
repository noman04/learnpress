@extends('admin.layouts.app')
@section('title', 'Courses')

@push('stylesheets')

@endpush
@section('content')

    <div class="col" style="margin-top: 50px">
        <h4 style="margin-bottom: 30px;font-weight: bold">Edit  Course</h4>

        <form action="{{ route('teachers.update', $teacher->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group col-xs-10 col-md-4 col-lg-4">
                <label for="name">Teacher Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{$teacher->name}}" >
            </div>
            <div class="form-group col-xs-10  col-md-4 col-lg-4">
                <label for="name">Email</label>
                <input type="email" class="form-control" name="email" id="email" value="{{$teacher->email}}">
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-4">
                <label for="name">Contact</label>
                <input type="text" class="form-control" name="contact" id="contact" value="{{$teacher->contact}}">
            </div>

            <div class="form-group col-xs-10 col-md-12 col-lg-12">
                <label for="comment">Description:</label>
                <textarea class="form-control" rows="3" id="description" name="description">{{$teacher->description}}
                </textarea>
            </div>

            <div class="form-group col-xs-10 col-md-12 col-lg-12">
                <label for="day">Select Course</label>
                <select selected  multiple="" class="form-control" id="course_id[]" name="course[]">
                    @foreach($course as $c)
                        <option value="{{ $c->id }}"   @foreach($teacher->courses as $cc){{$cc->pivot->course_id == $c->id ? 'selected': ''}}   @endforeach> {{ $c->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-4">
                <label for="name">Experience</label>
                <input type="text" class="form-control" name="experience" id="experience" value="{{$teacher->experience}}">
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-4">
                <label for="name">Address</label>
                <input type="text" class="form-control" name="address" id="address" value="{{$teacher->address}}">
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-4">
                <label for="name">Image</label>
                <img width="7%" class="" src="{{ URL::asset('storage/'.$teacher->thumbnail) }}">
                <input type="file" class="form-control" name="thumbnail" id="thumbnail">
            </div>


            <button type="submit" class="btn btn-info " style="width: 100%">Update Teacher</button>
        </form>
    </div>

@endsection


@push('scripts')

@endpush
