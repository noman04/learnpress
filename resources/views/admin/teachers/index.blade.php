@extends('admin.layouts.app')
@section('title', 'Teachers')

@push('stylesheets')

@endpush
@section('content')
    <!-- Hoverable rows start -->
    <section class="section">
        <div class="row" id="table-hover-row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <div class="row">
                           <div class="col-12 col-md-4">
                               <h3 class="text-bold-500" style="font-weight: bold">All Teachers</h3>
                           </div>
                           <div class="col-12 col-md-8 text-right">
                               <a href="{{ route('teachers.index') }}" class="btn btn-info">All Teacher</a>
                               <a href="{{ route('teachers.create') }}" class="btn btn-info">Add New Teacher</a>
                           </div>
                       </div>
                    </div>
                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    @if ($message = Session::get('deleted'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-content">
                        <div class="card-body">

                        </div>
                        <!-- table hover -->
                        @if(!$teachers->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
{{--                                    <th>Course</th>--}}
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Experience</th>
                                    <th>Image</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teachers as $teacher)
                                <tr>
                                    <td class="text-bold-500">{{ $teacher->id }}</td>
                                    <td class="text-bold-500">{{ $teacher->name }}</td>
{{--                                    <td class="text-bold-500">--}}
{{--                                            {{$teacher->CourseTeacher->id}}--}}

{{--                                    </td>--}}
                                    <td class="text-bold-500">{{ $teacher->email }}</td>
                                    <td class="text-bold-500">{{ $teacher->contact }}</td>
                                    <td class="text-bold-500">{{ $teacher->experience }} Year</td>
                                    <td class="text-bold-500"><img width="50" style="border-radius: 50%" height="50"  class="" src="{{ URL::asset('storage/'.$teacher->thumbnail) }}"></td>

                                      <td>
                                          <form action="{{ route('teachers.destroy',$teacher->id) }}" method="POST">
                                              <a class="btn btn-info" href="{{ route('teachers.show',$teacher->id) }}"><i class="fas fa-eye"></i></a>
                                              <a class="btn btn-success" href="{{ route('teachers.edit',$teacher->id) }}"><i class="fas fa-edit"></i></a>

                                              @csrf
                                              @method('DELETE')

                                              <button href="{{route('teachers.destroy', $teacher->id)}}"  type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                          </form>
                                      </td>
                                </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                            @else
                            <div class=" p-3">
                                <p class="alert text-center alert-info font-weight-bold">No Teacher Available</p>
                            </div>

                            @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hoverable rows end -->


@endsection


@push('scripts')

@endpush
